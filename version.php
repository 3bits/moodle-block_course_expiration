<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Shows course expiration date.
 *
 * @package    block_course_expiration
 * @version    1.0
 * @author     2021 3bits development team (3-bits.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 defined('MOODLE_INTERNAL') || die;

 // The component name.
 $plugin->component = "block_course_expiration";

 // Component version date (YYYYMMDDrr where rr is the release number).
 $plugin->version   = 2021111600;

 // Moodle required version is 3.10.0 or above.
 $plugin->requires  = 2020110900;

 // Component version using SemVer (https://semver.org).
 $plugin->release = '1.0';

 // Component maturity (do not use ALPHA or BETA versions in production sites).
 $plugin->maturity = MATURITY_STABLE;
