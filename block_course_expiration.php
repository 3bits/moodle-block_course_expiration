<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Shows course expiration date.
 *
 * @package    block_course_expiration
 * @version    1.0
 * @author     2021 3bits development team (3-bits.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class block_course_expiration extends block_base {
    public function init() {
        $this->title = get_string('pluginname', 'block_course_expiration');
    }

    public function applicable_formats() {
        return array('all' => false, 'course-view' => true);
    }

    public function has_config() {
        return false;
    }

    public function instance_allow_multiple() {
        return true;
    }

    public function instance_can_be_hidden() {
        return false;
    }

    public function get_content() {
        global $USER, $COURSE, $DB;
 
        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';

        if (isloggedin()) {
            if ($COURSE->id != SITEID) {
                $sql = 'SELECT ue.id, ue.timestart, ue.timeend
                    FROM mdl_user_enrolments ue
                    JOIN mdl_enrol e on ue.enrolid = e.id
                    WHERE ue.userid = ? AND e.courseid = ?';

                $records = $DB->get_records_sql($sql, array($USER->id, $COURSE->id));
                $student = reset($records);

                $this->title = get_string('expiretitle', 'block_course_expiration');

                $this->content->text .= '<div class="ce_main">'; // Start main div.

                if (isset($student->timeend)) {
                    $days = floor(($student->timeend - time()) / (24 * 60 * 60));
                } else {
                    $days = 0;
                }
                
                // Add different class color to the text depending on the days remaining.
                $ce_limit = 'ce_limit'; // Green.
                
                if ($days < 8) {
                    $ce_limit = 'ce_limit7'; // Ambar.
                }

                if ($days < 4) {
                    $ce_limit = 'ce_limit3'; // Red.
                }
                
                // if enrollment end date is bigger than zero then calculate the time remaining.
                if ($days > 0) {
                    $this->content->text .= '<p class="ce_date '.$ce_limit.'">'.date(get_string('strftimedate', 'block_course_expiration'), $student->timeend).'</p>';
                    $this->content->text .= '<p class="ce_time">'.'('.$days.' '.get_string('expireday', 'block_course_expiration').', '.floor((($student->timeend - time()) % (24 * 60 * 60)) / 3600).' '.get_string('expirehours', 'block_course_expiration').')';
                } else {
                    // if enrollment date is zero (unlimited time).
                    $this->content->text .= '<p class="ce_date ce_unlimited">'.get_string('enrolltext', 'block_course_expiration').'</p>';
                }
                $this->content->text .= '</div>'; // End main div.
            }
        }

        $this->content->footer = '';
        return $this->content;
    }
}
