<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course expiration date language strings (ES-ES).
 *
 * @package    block_course_expiration
 * @version    1.0
 * @author     2021 3bits development team (3-bits.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
    
    $string['pluginname'] = 'Vencimiento de la matrícula'; 
    $string['strftimedate'] = 'l, j \d\e F \d\e Y'; // Formato de fecha España.
	$string['expirelabel'] = 'La matriculación vence';
	$string['expiretitle'] = 'Vencimiento de la matrícula';
	$string['expireday'] = 'días';
	$string['expirehours'] = 'horas';
	$string['enrolltext'] = 'Sin límite de fecha';

    $string['course_expiration:addinstance'] = 'Añadir un nuevo bloque de Vencimiento de la matrícula al Área Personal';

