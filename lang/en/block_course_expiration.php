<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course expiration date language strings (EN-GB).
 *
 * @package    block_course_expiration
 * @version    1.0
 * @author     2021 3bits development team (3-bits.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
    
    $string['pluginname'] = 'Course Expiration'; 
    $string['strftimedate'] = 'l, j F Y'; // UK date format.
    $string['expirelabel'] = 'Your enrollment expires in: ';
    $string['expiretitle'] = 'Enrollment Expiration';
    $string['expireday'] = 'days';
    $string['expirehours'] = 'hours';
    $string['enrolltext'] = 'Unlimited date';

    $string['course_expiration:addinstance'] = 'Add a new Course Expiration block';

